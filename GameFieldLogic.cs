﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Life
{
    class GameFieldLogic
    {
        public int height
        {
            get;
            set;
        }
        public int width
        {
         get;
         set;
        }
        public bool[,] field;
        static Random rnd;
        
     
        public GameFieldLogic(int height,int width){
            this.height = height;
            this.width = width;
            field=new bool[height,width]; 
        }

        public void setCell(int hIndex,int wIndex){
            field[hIndex, wIndex] = !field[hIndex,wIndex];
         }
        public void setField(bool[,] field)
        {
            this.field = field;
        }

        private Boolean isAlive(int hIndex, int wIndex)
        {
            int aliveCellsAround = 0;
            try
            {
                if (field[hIndex - 1, wIndex - 1] == true) aliveCellsAround++;
                if (field[hIndex - 1, wIndex] == true) aliveCellsAround++;
                if (field[hIndex - 1, wIndex + 1] == true) aliveCellsAround++;
                if (field[hIndex, wIndex - 1] == true) aliveCellsAround++;
                if (field[hIndex, wIndex + 1] == true) aliveCellsAround++;
                if (field[hIndex + 1, wIndex - 1] == true) aliveCellsAround++;
                if (field[hIndex + 1, wIndex] == true) aliveCellsAround++;
                if (field[hIndex + 1, wIndex + 1] == true) aliveCellsAround++;
            }
            catch (IndexOutOfRangeException e) { }
            if (aliveCellsAround == 3 && field[hIndex, wIndex] == false) return true;
            if ((aliveCellsAround == 2 || aliveCellsAround == 3) && field[hIndex, wIndex] == true) return true;
            return false;
            
        }
        public void newGeneration()
        {
            bool[,] nextGenerationsField = new bool[height, width];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (isAlive(i, j)) nextGenerationsField[i, j] = true;
                    else nextGenerationsField[i, j] = false;
                }
            }
            
            field = nextGenerationsField;
        }
        public void randomGeneration()
        {
            bool[,] randomField = new bool[height, width];
            rnd = new Random();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    
                    if (rnd.NextDouble() > 0.75)
                    {
                        randomField[i, j] = true;

                    }
                    else randomField[i, j] = false;
                }
            }
            field = randomField;
        }
        public void emptyField()
        {
            bool[,] emptyField = new bool[height, width];
            field = emptyField;
        }

    }
}
