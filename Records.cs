﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace Life
{
    [Serializable]
    class Records
    {
        public String userName;
        // 15/20/30
        public int regimeFlag;
       public int score;


        public Records(String name, int reg, int score)
        {
            userName = name;
            regimeFlag = reg;
            this.score = score;
        }
        public Records(int i)
        {
            userName = "";
            regimeFlag = 0;
            this.score = 0;
        }
        public void writeRecord(int size,Records record)
        {
            Records[] lastRecords;
            string directory = Directory.GetCurrentDirectory();
            string fileDirectory = directory+"\\records\\records" + size.ToString() + ".bin";
            

            //Нет файла
            if (!File.Exists(fileDirectory))
            {
                //File.Create(directory + "\\records\\records" + size.ToString() + ".bin");

                FileStream fileStream = new FileStream(fileDirectory, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                using (fileStream)
                {
                    Records[] rec = new Records[10];
                    rec[0] = record;
                    rec[1] = new Records(0); rec[2] = new Records(0); rec[3] = new Records(0); rec[4] = new Records(0);
                    rec[5] = new Records(0); rec[6] = new Records(0); rec[7] = new Records(0); rec[8] = new Records(0); rec[9] = new Records(0);
                    IFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fileStream, rec);
                    MessageBox.Show("Новый рекорд сохранен!", "Сохранение", MessageBoxButtons.OK);
                    return;
                }

            }

            //Файл есть

            if (System.IO.File.Exists(fileDirectory))
            {
                lastRecords = record.getRecordsFromFile(fileDirectory);
                if (record.isTopRecord(record, lastRecords))
                {
                    Records[] newRecords = record.newRecords(record, lastRecords);
                    FileStream fileStream1 = new FileStream(fileDirectory, FileMode.Open, FileAccess.ReadWrite);
                    using (fileStream1)
                    {
                        IFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(fileStream1, newRecords);
                        MessageBox.Show("Рекорды сохранены!", "Сохранение", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("Игра завершена!", "", MessageBoxButtons.OK);
                }
            }


 }        
        public Boolean isTopRecord(Records record, Records[] records)
        {
            if (record.score < records[records.Length-1].score)
            {
                return false;
            }
            return true;
        }
        public Records[] newRecords(Records betterRec,Records[] lastRecords)
        {
            int N;
            int pos=0;
            if (lastRecords.Length<9) N=lastRecords.Length;
            else N=9;
            Records[] newRecords=new Records[9];
            for (int i = 0; i < N;i++)
            {
                if (betterRec.score > lastRecords[i].score)
                {
                    pos = i;
                    break;
                }             
            }
    //до позиции
            for (int i = 0; i < pos; i++)
            {
                newRecords[i] = lastRecords[i];
            }
                //позиция
            newRecords[pos] = betterRec;
                for (int i = pos+1; i < N; i++)
                {
                    newRecords[i]=lastRecords[i-1];

                }


            


            return newRecords;
        }


        public Records[] getRecordsFromFile(string directory)
        {                   
             FileStream fileStream = new FileStream(directory, FileMode.Open, FileAccess.ReadWrite);

                try
                {
                    {
                        IFormatter formatter = new BinaryFormatter();
                        using (fileStream)
                        {
                            return (Records[])formatter.Deserialize(fileStream);
                            // MessageBox.Show("Рекорды успешно загружены!", "Загрузка", MessageBoxButtons.OK);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка загрузки " + ex.Message);
                    return null;
                }
            
        }
    }
}
