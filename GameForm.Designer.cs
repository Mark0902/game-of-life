﻿namespace Life
{
    partial class GameForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gameField = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LowSpeed = new System.Windows.Forms.RadioButton();
            this.MiddleSpeed = new System.Windows.Forms.RadioButton();
            this.MaxSpeed = new System.Windows.Forms.RadioButton();
            this.random = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.score = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Label();
            this.startButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.gameField);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(76, -4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 566);
            this.panel1.TabIndex = 0;
            // 
            // gameField
            // 
            this.gameField.Location = new System.Drawing.Point(-3, 3);
            this.gameField.Name = "gameField";
            this.gameField.Size = new System.Drawing.Size(711, 560);
            this.gameField.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.LowSpeed);
            this.panel2.Controls.Add(this.MiddleSpeed);
            this.panel2.Controls.Add(this.MaxSpeed);
            this.panel2.Controls.Add(this.random);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.score);
            this.panel2.Controls.Add(this.timer);
            this.panel2.Controls.Add(this.startButton);
            this.panel2.Location = new System.Drawing.Point(0, -4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(70, 566);
            this.panel2.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Image = global::Life.Properties.Resources.refresh_page_arrow_shape_icon_icons_com_53907_48_;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(8, 501);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 54);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // LowSpeed
            // 
            this.LowSpeed.AutoSize = true;
            this.LowSpeed.Location = new System.Drawing.Point(3, 235);
            this.LowSpeed.Name = "LowSpeed";
            this.LowSpeed.Size = new System.Drawing.Size(46, 17);
            this.LowSpeed.TabIndex = 8;
            this.LowSpeed.TabStop = true;
            this.LowSpeed.Text = "Мин";
            this.LowSpeed.UseVisualStyleBackColor = true;
            this.LowSpeed.CheckedChanged += new System.EventHandler(this.LowSpeed_CheckedChanged);
            // 
            // MiddleSpeed
            // 
            this.MiddleSpeed.AutoSize = true;
            this.MiddleSpeed.Location = new System.Drawing.Point(4, 210);
            this.MiddleSpeed.Name = "MiddleSpeed";
            this.MiddleSpeed.Size = new System.Drawing.Size(56, 17);
            this.MiddleSpeed.TabIndex = 7;
            this.MiddleSpeed.TabStop = true;
            this.MiddleSpeed.Text = "Средн";
            this.MiddleSpeed.UseVisualStyleBackColor = true;
            this.MiddleSpeed.CheckedChanged += new System.EventHandler(this.MiddleSpeed_CheckedChanged);
            // 
            // MaxSpeed
            // 
            this.MaxSpeed.AutoSize = true;
            this.MaxSpeed.Location = new System.Drawing.Point(4, 187);
            this.MaxSpeed.Name = "MaxSpeed";
            this.MaxSpeed.Size = new System.Drawing.Size(52, 17);
            this.MaxSpeed.TabIndex = 0;
            this.MaxSpeed.TabStop = true;
            this.MaxSpeed.Text = "Макс";
            this.MaxSpeed.UseVisualStyleBackColor = true;
            this.MaxSpeed.CheckedChanged += new System.EventHandler(this.MaxSpeed_CheckedChanged);
            // 
            // random
            // 
            this.random.Location = new System.Drawing.Point(12, 90);
            this.random.Name = "random";
            this.random.Size = new System.Drawing.Size(46, 23);
            this.random.TabIndex = 1;
            this.random.Text = "~~~";
            this.random.UseVisualStyleBackColor = true;
            this.random.Click += new System.EventHandler(this.random_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 39);
            this.label1.TabIndex = 5;
            this.label1.Text = "Изменение\r\nскорости\r\n цикла";
            // 
            // score
            // 
            this.score.AutoSize = true;
            this.score.Location = new System.Drawing.Point(10, 377);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(13, 13);
            this.score.TabIndex = 2;
            this.score.Text = "0";
            // 
            // timer
            // 
            this.timer.AutoSize = true;
            this.timer.Location = new System.Drawing.Point(5, 339);
            this.timer.Name = "timer";
            this.timer.Size = new System.Drawing.Size(55, 26);
            this.timer.TabIndex = 1;
            this.timer.Text = "Прожито \r\n  циклов";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(3, 16);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(64, 53);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Старт";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "GameForm";
            this.Text = "Игра Жизнь";
            this.Load += new System.EventHandler(this.GameForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Label timer;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel gameField;
        private System.Windows.Forms.Button random;
        private System.Windows.Forms.RadioButton LowSpeed;
        private System.Windows.Forms.RadioButton MiddleSpeed;
        private System.Windows.Forms.RadioButton MaxSpeed;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

