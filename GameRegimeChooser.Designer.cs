﻿namespace Life
{
    partial class GameRegimeChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.regime15 = new System.Windows.Forms.Button();
            this.regime20 = new System.Windows.Forms.Button();
            this.regime30 = new System.Windows.Forms.Button();
            this.regime50 = new System.Windows.Forms.Button();
            this.colorText = new System.Windows.Forms.Label();
            this.infoLabel = new System.Windows.Forms.Label();
            this.currentColor = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // regime15
            // 
            this.regime15.Location = new System.Drawing.Point(18, 26);
            this.regime15.Name = "regime15";
            this.regime15.Size = new System.Drawing.Size(75, 23);
            this.regime15.TabIndex = 0;
            this.regime15.Text = "15x15";
            this.regime15.UseVisualStyleBackColor = true;
            this.regime15.Click += new System.EventHandler(this.regime15_Click);
            this.regime15.MouseLeave += new System.EventHandler(this.regime15_MouseLeave);
            this.regime15.MouseHover += new System.EventHandler(this.regime15_MouseHover);
            // 
            // regime20
            // 
            this.regime20.Location = new System.Drawing.Point(126, 26);
            this.regime20.Name = "regime20";
            this.regime20.Size = new System.Drawing.Size(75, 23);
            this.regime20.TabIndex = 1;
            this.regime20.Text = "20x20";
            this.regime20.UseVisualStyleBackColor = true;
            this.regime20.Click += new System.EventHandler(this.regime20_Click);
            this.regime20.MouseLeave += new System.EventHandler(this.regime20_MouseLeave);
            this.regime20.MouseHover += new System.EventHandler(this.regime20_MouseHover);
            // 
            // regime30
            // 
            this.regime30.Location = new System.Drawing.Point(237, 26);
            this.regime30.Name = "regime30";
            this.regime30.Size = new System.Drawing.Size(75, 23);
            this.regime30.TabIndex = 2;
            this.regime30.Text = "30x30";
            this.regime30.UseVisualStyleBackColor = true;
            this.regime30.Click += new System.EventHandler(this.regime30_Click);
            this.regime30.MouseLeave += new System.EventHandler(this.regime30_MouseLeave);
            this.regime30.MouseHover += new System.EventHandler(this.regime30_MouseHover);
            // 
            // regime50
            // 
            this.regime50.Location = new System.Drawing.Point(350, 26);
            this.regime50.Name = "regime50";
            this.regime50.Size = new System.Drawing.Size(75, 23);
            this.regime50.TabIndex = 3;
            this.regime50.Text = "50x50";
            this.regime50.UseVisualStyleBackColor = true;
            this.regime50.Click += new System.EventHandler(this.regime50_Click);
            this.regime50.MouseLeave += new System.EventHandler(this.regime50_MouseLeave);
            this.regime50.MouseHover += new System.EventHandler(this.regime50_MouseHover);
            // 
            // colorText
            // 
            this.colorText.AutoSize = true;
            this.colorText.Location = new System.Drawing.Point(6, 113);
            this.colorText.Name = "colorText";
            this.colorText.Size = new System.Drawing.Size(70, 26);
            this.colorText.TabIndex = 4;
            this.colorText.Text = "Цвет живой \r\n   клетки";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Location = new System.Drawing.Point(40, 64);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(73, 13);
            this.infoLabel.TabIndex = 5;
            this.infoLabel.Text = "//Пояснение";
            // 
            // currentColor
            // 
            this.currentColor.Enabled = false;
            this.currentColor.Location = new System.Drawing.Point(22, 151);
            this.currentColor.Name = "currentColor";
            this.currentColor.Size = new System.Drawing.Size(28, 22);
            this.currentColor.TabIndex = 6;
            this.currentColor.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(100, 151);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(28, 22);
            this.button2.TabIndex = 7;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(126, 151);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(28, 22);
            this.button3.TabIndex = 8;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(152, 151);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(28, 22);
            this.button4.TabIndex = 9;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(178, 151);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(28, 22);
            this.button5.TabIndex = 10;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(204, 151);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(28, 22);
            this.button6.TabIndex = 11;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(230, 151);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(28, 22);
            this.button7.TabIndex = 12;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(256, 151);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(28, 22);
            this.button8.TabIndex = 13;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(282, 151);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(28, 22);
            this.button9.TabIndex = 14;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(308, 151);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(28, 22);
            this.button10.TabIndex = 15;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(334, 151);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(28, 22);
            this.button11.TabIndex = 16;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(360, 151);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(28, 22);
            this.button12.TabIndex = 17;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // GameRegimeChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 194);
            this.ControlBox = false;
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.currentColor);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.colorText);
            this.Controls.Add(this.regime50);
            this.Controls.Add(this.regime30);
            this.Controls.Add(this.regime20);
            this.Controls.Add(this.regime15);
            this.Name = "GameRegimeChooser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор режима игры";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button regime15;
        private System.Windows.Forms.Button regime20;
        private System.Windows.Forms.Button regime30;
        private System.Windows.Forms.Button regime50;
        private System.Windows.Forms.Label colorText;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button currentColor;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
    }
}