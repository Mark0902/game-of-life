﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Life
{
    public partial class MenuForm : Form
    {
        System.Drawing.Color color;
        
        public MenuForm()
        {
            InitializeComponent();
            //обычный цвет - синий
            color=System.Drawing.Color.Blue;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            GameRegimeChooser grc = new GameRegimeChooser(this);
            this.Hide();
            grc.Show();

            
            
            //GameForm gameForm = new GameForm(this,size,color);
           // gameForm.Show();
            //this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RulesForm rules = new RulesForm();
            rules.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DemonstrationForm demo = new DemonstrationForm();
            demo.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RecordsForm records = new RecordsForm();
            records.Show();
        }

        private void DemonstrationButton_Click(object sender, EventArgs e)
        {
            DemonstrationForm dmf = new DemonstrationForm();
            dmf.Show();
        }
    }
}
