﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Life
{
    public partial class GameForm : Form
    {
        MenuForm menu;
        System.Drawing.Color cellColor;

        int scoreCounter = 0;
        GameFieldLogic gameFieldLogic;
        bool gameFlag = false;
        List<Boolean[,]> listOfFields = new List<bool[,]>();
        int height = 0;
        int width = 0;
        int size = 0;

        public GameForm(MenuForm m, int size, System.Drawing.Color color)
        {
            InitializeComponent();
            this.size = size;
            cellColor = color;
            menu = m;
            label1.Visible = false; 
            MiddleSpeed.Checked = true;
            MaxSpeed.Visible = false;
            MiddleSpeed.Visible = false;
            LowSpeed.Visible = false;
            Draw(size, size);
            score.ForeColor = System.Drawing.Color.Red;
            score.Font = new System.Drawing.Font("Comic Sans", 20);

        }
        //Отрисовка поля
        public void Draw(int xCells, int yCells)
        {
            Button cell;
            int fieldsWidth = gameField.Size.Width;
            int fieldsHeight = gameField.Size.Height;

            int xSize = fieldsWidth / xCells;
            int ySize = fieldsHeight / yCells;

            System.Drawing.Size cellSize;
            System.Drawing.Size cellControl;

            cellControl = new System.Drawing.Size(xSize * (xCells - 1), ySize * (yCells - 1));
            int xSmoothing = 0, ySmoothing = 0;
            if (xCells == 15) { xSmoothing = 5; ySmoothing = 5; }
            if (xCells == 20) { xSmoothing = 7; ySmoothing = 0; }
            if (xCells == 30) { xSmoothing = 10; ySmoothing = 10; }
            if (xCells == 50) { xSmoothing = 5; ySmoothing = 7; }

            for (int y = 0; y < yCells; y++)
            {
                for (int x = 0; x < xCells; x++)
                {
                    cellSize = new System.Drawing.Size(xSize, ySize);
                    cell = new Button();
                    cell.Name = x.ToString() + "," + y.ToString();
                    cell.Tag = x.ToString() + "," + y.ToString();
                    cell.Size = cellSize;
                    //15,15 x+5 y+5
                    //20x20 x+5,y==
                    //30x30 x+10,y+10                    
                    //50x50 +5,+7
                    cell.Location = new Point(x * xSize + xSmoothing, y * ySize + ySmoothing);
                    cell.Click += new System.EventHandler(this.cell_Click);
                    gameField.Controls.Add(cell);
                }
            }
            gameFieldLogic = new GameFieldLogic(yCells, xCells);
            listOfFields.Add(gameFieldLogic.field);
            height = yCells;
            width = xCells;
        }

        private void getCoordinates(string coordString, ref string x, ref string y)
        {
            string[] arrayForSplit;
            arrayForSplit = coordString.Split(",".ToCharArray());
            x = arrayForSplit[0].ToString();
            y = arrayForSplit[1].ToString();
        }
        //Живая клетка отображается синим цветом
        private void repaint(bool active, Button cell)
        {
            if (active)
            {
                cell.BackColor = cellColor;
            }
            else cell.BackColor = System.Drawing.Color.White;

        }
        private void fillingField(GameFieldLogic gameField)
        {
            string xCor = "";
            string yCor = "";
            foreach (Button cell in this.gameField.Controls)
            {
                getCoordinates(cell.Name, ref xCor, ref yCor);
                repaint(gameField.field[int.Parse(yCor), int.Parse(xCor)], cell);
            }
        }

        private void cell_Click(object sender, EventArgs e)
        {
            Button cell;
            string position;
            string xCor = "";
            string yCor = "";

            cell = (Button)sender;
            position = cell.Tag.ToString();
            getCoordinates(position, ref xCor, ref yCor);
            gameFieldLogic.setCell(int.Parse(yCor), int.Parse(xCor));
            fillingField(gameFieldLogic);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!gameFlag)
            {
                scoreCounter = 0;
                timer1.Enabled = true;
                label1.Visible = true;
                MaxSpeed.Visible = true;
                MiddleSpeed.Visible = true;
                LowSpeed.Visible = true;
                gameField.Enabled = false;
                random.Enabled = false;
                random.Visible = false;
                startButton.Text = "Стоп";
                gameFlag = !gameFlag;
            }
            else
            {
                timer1.Enabled = false;
                label1.Visible = false;
                MaxSpeed.Visible = false;
                MiddleSpeed.Visible = false;
                LowSpeed.Visible = false;
                gameField.Enabled = true;
                random.Enabled = true;
                random.Visible = true;
                startButton.Text = "Старт";
                gameFlag = !gameFlag;
                //очистка поля
                scoreCounter = 0;
                gameFieldLogic.emptyField();
                fillingField(gameFieldLogic);

            }
        }


        private Boolean fieldCompare(bool[,] currentFeild, bool[,] field)
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (currentFeild[i, j] != field[i, j]) return false;
                }
            }
            return true;
        }

        //Проверяет наличие текущего поля в базе,позвращает true если не находит
        private Boolean checkStatus(bool[,] currentField, List<Boolean[,]> field)
        {
            foreach (bool[,] f in field)
            {
                if (fieldCompare(f, currentField)) return false;
            }
            return true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            gameFieldLogic.newGeneration();

            if (checkStatus(gameFieldLogic.field, listOfFields))
            {
                fillingField(gameFieldLogic);
                scoreCounter++;
                score.Text = scoreCounter.ToString();
                listOfFields.Add(gameFieldLogic.field);
            }
            else
            {
                timer1.Enabled = false;
                if (scoreCounter == 0) { MessageBox.Show("Пустое поле"); }
                else
                    if (size != 50)
                {
                    //Проверка на наличие рекордов
                    Records newRecord = new Records("M@RK", size, scoreCounter);
                    newRecord.writeRecord(size, newRecord);

                    // MessageBox.Show("Игра завершена.Ваш счет " + score.Text.ToString());
                }
                else
                {
                    MessageBox.Show("Игра завершена.Ваш счет " + score.Text.ToString());
                }
                scoreCounter = 0;
                listOfFields.Clear();
                score.Text = scoreCounter.ToString();
            }

        }

        private void random_Click(object sender, EventArgs e)
        {
            gameFieldLogic.randomGeneration();
            fillingField(gameFieldLogic);
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            // menu.Close();
        }

        private void MaxSpeed_CheckedChanged(object sender, EventArgs e)
        {
            if (MaxSpeed.Checked == true)
            {
                MiddleSpeed.Checked = false;
                LowSpeed.Checked = false;
                //MaxSpeed.Checked = true;
                timer1.Interval = 100;
                MaxSpeed.Enabled = false;
                MiddleSpeed.Enabled = true;
                LowSpeed.Enabled = true;
            }
        }

        private void MiddleSpeed_CheckedChanged(object sender, EventArgs e)
        {
            if (MiddleSpeed.Checked == true)
            {
                //MiddleSpeed.Checked = true;
                LowSpeed.Checked = false;
                MaxSpeed.Checked = false;
                timer1.Interval = 500;
                MiddleSpeed.Enabled = false;
                MaxSpeed.Enabled = true;
                LowSpeed.Enabled = true;
            }

        }

        private void LowSpeed_CheckedChanged(object sender, EventArgs e)
        {
            if (LowSpeed.Checked == true)
            {
                MiddleSpeed.Checked = false;
                //LowSpeed.Checked = true;
                MaxSpeed.Checked = false;
                timer1.Interval = 1000;
                LowSpeed.Enabled = false;
                MaxSpeed.Enabled = true;
                MiddleSpeed.Enabled = true;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
            menu.Show();
        }
        //Разобраться с процессом
        /* private void GameForm_Closing(object sender, FormClosedEventArgs e)
        {
            menu.Close();
        }
         */
    }
}
