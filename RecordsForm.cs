﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace Life
{
    public partial class RecordsForm : Form
    {
        Records[] currRec;
        string directory;
        string fileDirectory;
        Records exRec;

        public RecordsForm()
        {
            InitializeComponent();
            exRec = new Records(0);
            directory = Directory.GetCurrentDirectory();
            fileDirectory = directory + "\\records\\records" + 15.ToString() + ".bin";
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            fileDirectory = directory + "\\records\\records" + 15.ToString() + ".bin";
            nameLabel.Text = "";
            scoreLabel.Text = "";
            if (!File.Exists(fileDirectory))
            {
                nameLabel.Text = "Рекордов пока нет";
                return;
            }
            currRec = exRec.getRecordsFromFile(fileDirectory);
            
            for (int i = 0; i < currRec.Length; i++)
            {
                nameLabel.Text = nameLabel.Text + (i+1).ToString() + ")" + currRec[i].userName + "\n";
                scoreLabel.Text = scoreLabel.Text + currRec[i].score + "\n";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            fileDirectory = directory + "\\records\\records" + 20.ToString() + ".bin";
            nameLabel.Text = "";
            scoreLabel.Text = "";
            if (!File.Exists(fileDirectory))
            {
                nameLabel.Text = "Рекордов пока нет";
                return;
            }
            currRec = exRec.getRecordsFromFile(fileDirectory);
                        
                        for (int i = 0; i < currRec.Length; i++)
                        {
                            nameLabel.Text = nameLabel.Text + (i+1).ToString() + ")" + currRec[i].userName + "\n";
                            scoreLabel.Text = scoreLabel.Text + currRec[i].score + "\n";
                        }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            fileDirectory = directory + "\\records\\records" + 30.ToString() + ".bin";
            nameLabel.Text = "";
            scoreLabel.Text = "";
            if (!File.Exists(fileDirectory))
            {
                nameLabel.Text = "Рекордов пока нет";
                return;
            }
            currRec = exRec.getRecordsFromFile(fileDirectory);
                        
                        for (int i = 0; i < currRec.Length; i++)
                        {
                            nameLabel.Text = nameLabel.Text + (i+1).ToString()+")"+currRec[i].userName + "\n";
                            scoreLabel.Text = scoreLabel.Text + currRec[i].score + "\n";
                        }
        }
        }
    }
