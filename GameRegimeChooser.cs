﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Life
{
    public partial class GameRegimeChooser : Form
    {
        System.Drawing.Color curColor;
        MenuForm menu;
        public GameRegimeChooser(MenuForm m)
        {
            InitializeComponent();
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;

            menu=m;
            infoLabel.Font = new System.Drawing.Font("Comic Sans", 10);
            infoLabel.Text = "";

            curColor = System.Drawing.Color.Blue;
            currentColor.BackColor = curColor;
            button2.BackColor = System.Drawing.Color.Blue;
            button3.BackColor = System.Drawing.Color.Red;
            button4.BackColor = System.Drawing.Color.Yellow;
            button5.BackColor = System.Drawing.Color.Brown;
            button6.BackColor = System.Drawing.Color.Green;
            button7.BackColor = System.Drawing.Color.Orange;
            button8.BackColor = System.Drawing.Color.Pink;
            button9.BackColor = System.Drawing.Color.Violet;
            button10.BackColor = System.Drawing.Color.Black;
            button11.BackColor = System.Drawing.Color.DimGray;
            button12.BackColor = System.Drawing.Color.Aquamarine;           
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button2.BackColor;
            curColor = button2.BackColor;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button3.BackColor; ;
            curColor = button3.BackColor; ;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button4.BackColor;
            curColor = button4.BackColor;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button5.BackColor;
            curColor = button5.BackColor;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button6.BackColor;
            curColor = button6.BackColor;
        }
        private void button7_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button7.BackColor;
            curColor = button7.BackColor;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button8.BackColor;
            curColor = button8.BackColor;
        }
        private void button9_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button9.BackColor;
            curColor = button9.BackColor;
        }
        private void button10_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button10.BackColor;
            curColor = button10.BackColor;
        }
        
        private void button11_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button11.BackColor;
            curColor = button11.BackColor;
        }
        private void button12_Click(object sender, EventArgs e)
        {
            currentColor.BackColor = button12.BackColor;
            curColor = button12.BackColor;
        }

        private void regime15_Click(object sender, EventArgs e)
        {
            GameForm gf = new GameForm(menu,15, curColor);
            this.Close();
            gf.Show();
        }

        private void regime20_Click(object sender, EventArgs e)
        {
            GameForm gf = new GameForm(menu,20, curColor);
            this.Close();
            gf.Show();
        }

        private void regime30_Click(object sender, EventArgs e)
        {
            GameForm gf = new GameForm(menu,30, curColor);
            this.Close();
            gf.Show();
        }

        private void regime50_Click(object sender, EventArgs e)
        {
            GameForm gf = new GameForm(menu,50, curColor);
            this.Close();
            gf.Show();

        }









        ///
        private void regime15_MouseHover(object sender, EventArgs e)
        {
            infoLabel.Text = "Самый простой режим";
        }
        private void regime20_MouseHover(object sender, EventArgs e)
        {
            infoLabel.Text = "Оптимально";
        }
        private void regime30_MouseHover(object sender, EventArgs e)
        {
            infoLabel.Text = "Сложно";
        }
        private void regime50_MouseHover(object sender, EventArgs e)
        {
            infoLabel.Text = "Большая вселенная \nБез записи рекордов";
        }


        private void regime15_MouseLeave(object sender, EventArgs e)
        {
            infoLabel.Text = "";
        }
        private void regime20_MouseLeave(object sender, EventArgs e)
        {
            infoLabel.Text = "";
        }
        private void regime30_MouseLeave(object sender, EventArgs e)
        {
            infoLabel.Text = "";
        }
        private void regime50_MouseLeave(object sender, EventArgs e)
        {
            infoLabel.Text = "";
        }
         
        

    }
}
